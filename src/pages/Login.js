import {useEffect, useState} from 'react'
import {Form, Button} from 'react-bootstrap'

export default function Login(){

	const [password,setPassword]=useState('')
	const [email,setEmail]=useState('')

	const [isActive, setIsActive]=useState(false)

	useEffect(()=>{
		if((email !=='' && password!== '') && (password === password)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email,password])

	function loginUser(e){
			if(email==='admin@mail.com' && password==='admin'){
				alert('Logged in Successfully!')
			} else {

			}
	}

	return(

		<Form onSubmit = {(e)=>loginUser(e)}>
			<Form.Group>
				<Form.Label>
					Login
				</Form.Label>
				<Form.Label>
					Email Address:
				</Form.Label>
				<Form.Control 
					type='email'
					placeholder='Enter Email'
					value={email}
					onChange={e=>setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId='password'>
				<Form.Label>
					Password:
				</Form.Label>
				<Form.Control
					type='password'
					placeholder='Password'
					value={password}
					onChange={e=>setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ?
			<Button variant='success' type='submit' id='submitBtn' >Login</Button>
			:
			<Button variant='warning' type='submit' id='submitBtn' disabled>Login</Button>
			}
		</Form>
		)
}