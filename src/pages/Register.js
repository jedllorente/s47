import {useState, useEffect} from 'react'

import {Form, Button} from 'react-bootstrap'


export default function Register(){

	// State hooks to store the values of the input fields
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')

	// State to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)

	console.log('email: ' + email)
	console.log('password1: ' + password1)
	console.log('password2: ' + password2)


	function registerUser(e){

		e.preventDefault();
		
		// Clears the input field
		setEmail('')
		setPassword1('')
		setPassword2('')

		alert('Thank you for registering!')
	}

	useEffect(()=> {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){

			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email,password1,password2])

	return(
		<Form onSubmit={(e)=>registerUser(e)}>
			<Form.Group>
			<h2>Register</h2>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type = 'email'
					placeholder = 'Please enter your email here'
					value= {email}
					onChange= {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
				   We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type= 'password'
					placeholder = 'Please input your password here'
					value = {password1}
					onChange = {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control
					type= 'password'
					placeholder = 'Please verify your Password'
					value = {password2}
					onChange = {e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>
			{	isActive ? 
				<Button variant='primary' type='submit' id='submitBtn'>
							Register
				</Button>

				: 
				<Button variant='danger' type='submit' id='submitBtn' disabled>
							Register
				</Button>

			}
		</Form>

	)
}