import {Alert} from 'react'
import {useState} from 'react'
import {Card, Button} from 'react-bootstrap'

export default function CourseCard({courseProp}) {

console.log(courseProp)

const {name, descriptoin, price} = courseProp

const [count, setCount]= useState(1)

function enroll(){
    if(count < 30){
        setCount(count +1)
    }else {
        alert('no more seats')
    }

    console.log('Enrollees' + count)
}

    return (
        <Card className ='my-3'>
            <Card.Body>
                <Card.Title>{courseProp.name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{courseProp.description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{courseProp.price}</Card.Text>
                <Card.Text>{count} Enrollees</Card.Text>
                <Button variant="primary" onClick={enroll} >Enroll</Button>
            </Card.Body>
        </Card>
    )
}
